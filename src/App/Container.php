<?php

namespace App;

use Noodlehaus\Config;
use Slim\Views\PhpRenderer;
class Container extends \Slim\Container
{
    public function __construct(array $values = [])
    {
        parent::__construct(static::getServices());
    }

    protected static function getServices()
    {
        return [
            'settings' => [
                'displayErrorDetails' => true,
            ],
            'config' => function () {
                return new Config(__DIR__ . '/config.json');
            },
            'renderer' => function ($container) {
                return new PhpRenderer(ROOT_PATH . '/public');
            },
            'redis' => function ($container) {
                return new \Predis\Client($container->get('config')->get('redis'));
            },
            'memcache' => function ($container) {
                $memcache = new \Memcache;
                $memcache->connect(
                    $container->get('config')->get('memcache.host'),
                    $container->get('config')->get('memcache.port')
                );
                return $memcache;
            },
            'mysql' => function($container) {
                return new \PDO(
                    $container->get('config')->get('mysql.dsn'),
                    $container->get('config')->get('mysql.user'),
                    $container->get('config')->get('mysql.pass')
                );
            },
            'logger' =>  function($container) {
                return new Logger($container);
            },
            'suggestions' => function($container) {
                return new Suggestion($container);
            },
        ];
    }
}
