<?php

namespace App;

class Logger
{
    const RKEY_QUEUE = 'requestLog';
    const LOG_TABLE = 'query_log';
    const CHUNK_SIZE = 200;

    protected $_container;

    public function __construct(\Slim\Container $container)
    {
        $this->_container = $container;
    }

    public function pushToQueue($toLog)
    {
        $this->_container->redis->lpush(static::RKEY_QUEUE, json_encode($toLog));
    }

    public function popAllFromQueue()
    {
        $data = [];

        $pipeline = $this->_container->redis->pipeline();
        $pipeline->lrange(static::RKEY_QUEUE, 0, -1);
        $pipeline->del(static::RKEY_QUEUE);

        $res = $pipeline->execute();

        if (!empty($res[0])) {
            foreach ($res[0] as $row) {
                $data[] = json_decode($row, true);
            }
        }

        return $data;
    }

    public function cron()
    {
        $logData = $this->popAllFromQueue();

        if ($logData) {
            $chunks = array_chunk($logData, static::CHUNK_SIZE);

            $dbh = $this->_container->mysql;

            foreach ($chunks as $chunk) {
                $toInsert = [];

                foreach ($chunk as $logItem) {
                    list($request, $responce, $time) = $logItem;
                    $toInsert[] = sprintf("'%s', '%s', %d", $request, $responce, $time);
                }

                $insertString = implode('), (', $toInsert);
                $query = sprintf("INSERT INTO `%s` (request, responce, time) VALUES (%s)", static::LOG_TABLE, $insertString);

                $dbh->query($query);
            }
        }
    }
}
