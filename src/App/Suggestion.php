<?php

namespace App;

class Suggestion
{
    const SUGGETION_URL = 'https://suggest-maps.yandex.ru/suggest-geo?v=5&search_type=tp&lang=ru_RU&n=6&origin=jsapi2Geocoder&callback=&part=';
    const MKEY_QUERY = 'query:';
    const MTTL_QUERY = 3600;

    protected $_container;

    public function __construct(\Slim\Container $container)
    {
        $this->_container = $container;
    }

    public function getSuggetion($query)
    {
        $suggestion = $this->_container->memcache->get(static::MKEY_QUERY . $query);

        if ($suggestion === false) {
            $ch = curl_init();
            $req = static::SUGGETION_URL . $query;
            curl_setopt($ch, CURLOPT_URL, $req);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);

            if (!$output) {
                return false;
            }

            $this->_addToLog([$req, $output, time()]);

            $suggestion = json_decode($output, true);

            $suggestion = $suggestion[1];

            $suggestion = array_column($suggestion, 1);

            $this->_container->memcache->set(
                static::MKEY_QUERY . $query,
                json_encode($suggestion),
                0,
                static::MTTL_QUERY
            );
        } else {
            $suggestion = json_decode($suggestion, true);
        }

        return $suggestion;
    }

    protected function _addToLog($logData)
    {
        $this->_container->logger->pushToQueue($logData);
    }
}
