<?php

defined('ROOT_PATH')
    || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../'));

$composer = realpath(ROOT_PATH . '/vendor') . '/autoload.php';
if (is_file($composer)) {
    $autoloader = require_once $composer;
}

use App\Container;

$container = new Container();

$container->logger->cron();
