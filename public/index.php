<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use App\Container;

defined('ROOT_PATH')
    || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../'));

$composer = realpath(ROOT_PATH . '/vendor') . '/autoload.php';
if (is_file($composer)) {
    $autoloader = require_once $composer;
}

$container = new Container();

$app = new \Slim\App($container);

$app->get('/', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, "main.html");
});

$app->get('/api/{query}', function (Request $request, Response $response, array $args) use ($app) {
    $query = $args['query'];

    if ($query && $suggestion = $this->suggestions->getSuggetion($query)) {
        return $response->withJson(['suggestions' => $suggestion]);
    }

    return $response->withJson(['error' => 'empty']);
});

$app->run();
